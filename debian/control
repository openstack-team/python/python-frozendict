Source: python-frozendict
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Andrej Shadura <andrewsh@debian.org>,
 Edward Betts <edward@4angle.com>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-setuptools,
Build-Depends-Indep:
 python3-pytest <!nocheck>,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/openstack-team/python/python-frozendict
Vcs-Git: https://salsa.debian.org/openstack-team/python/python-frozendict.git
Homepage: https://github.com/Marco-Sulla/python-frozendict

Package: python3-frozendict
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Description: immutable dictionary
 Frozendict is an immutable wrapper around dictionaries that implements the
 complete mapping interface. It can be used as a drop-in replacement for
 dictionaries where immutability is desired.
 .
 Of course, this is Python, and you can still poke around the object's
 internals if you want.
 .
 The frozendict constructor mimics dict, and all of the expected interfaces
 (iter, len, repr, hash, getitem) are provided. Note that a frozendict does not
 guarantee the immutability of its values, so the utility of hash method is
 restricted by usage.
 .
 The only difference is that the copy() method of frozendict takes variable
 keyword arguments, which will be present as key/value pairs in the new,
 immutable copy.
